'use strict';

const express    = require('express');
const bodyParser = require('body-parser');

// Constants
const PORT = 8080; //process.env.npm_package_config_port;

// App
const app = express();

app.get('/', function (req, res) {
  res.send('NodeJS running on EC2\n');
});

app.get('/hi', function (req, res) {
  res.send('Hello world!\n');
});

app.listen(PORT);
console.log('Running on http://localhost:' + PORT);
