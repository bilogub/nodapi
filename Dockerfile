FROM node:alpine
RUN mkdir /nodapi
WORKDIR /nodapi
ADD . /nodapi
RUN npm install
EXPOSE 8080
